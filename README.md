# ultrasonic-distance-sensor

Basic use case for the Raspberry PI (Model 3 B+) GPIO ports , this ports read sensor information from a SRF05 ultrasonic sensor, and depending of distance sensed, turn on or turn off a green/yellow/red LED as a indicator of distance range. 

This project is based on the official tutorial in the next link: https://projects.raspberrypi.org/en/projects/physical-computing/14